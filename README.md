Pourquoi avoir choisi une infrastructere Serverless ?

J'ai choisi ce type d'infrastructure serverless car ce modèle ne neccessite aucune gestion et de configuration de serveurs. Cela m'as permis de poser directement les fonctionnalités demander et commencer le developpement sur le projet en seulement quelques clics. C'est un modèle extremement performant qui supporte plusieurs languages de programmation et qui est fiable. De plus c'est un avantage pour le client il ne paye que ce qu'il consomme. Aucune instance de deployer   

Concernant la base de données non relationnelle j'ai choisi DynamoDB. Il s’agit d’une base de données multirégion et multimaître full manager qui fournit une latence constante de quelques millisecondes et un système de sécurité, de sauvegarde et de restauration intégré, ainsi que de mise en cache en mémoire.

Ce service est idéal pour les applications mobiles, Web, de jeux, de technologie publicitaire nécessitant un accès faible latence aux données. Il suffit de seulement quelques clics pour créer notre table.

Pour pouvoir traiter les requêtes de l'application j'ai bien evidemment mis en place un API Gateway avec des Lambdas qui rendent possible l'execution du code dans un monde Serverless. C'est les 2 service vital chez amazon pour faire une infrastructure de ce type. API Gateway permet de mettre en relation les fonctionnalités developpé sur les lambdas. 

Pour le stockage j'ai choisi le service S3 pour y héberger le front, celui ci me permet de versionner l'application en cas d'une mauvaise manipulation. 


Voir le front
https://s3-eu-west-1.amazonaws.com/benprojectapi/website/mysite.html       

  
  

